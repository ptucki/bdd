<?php

use App\Doctor;

$factory->define(Doctor::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'specialization' => $faker->randomElement(['kardiolog', 'okulista', 'internista', 'urolog']),
    ];
});
