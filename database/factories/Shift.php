<?php

use App\Doctor;
use App\Shift;
use Carbon\Carbon;

$factory->define(Shift::class, function (Faker\Generator $faker) {
    $startsAt = Carbon::make($faker->dateTime);
    return [
        'cost' => $faker->numerify('###.##'),
        'doctor_id' => fn() => factory(Doctor::class)->create()->id,
        'ends_at' => $startsAt->addHours($faker->numberBetween(4, 12)),
        'starts_at' => $startsAt,
    ];
});
