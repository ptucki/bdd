<?php

use App\User;

$factory->define(User::class, function (Faker\Generator $faker) {
    return [
        'api_token' => $faker->password,
        'email' => $faker->email,
        'medical_plan' => $faker->randomElement(['basic', 'silver', 'gold']),
        'name' => $faker->name,
        'password' => $faker->password,
    ];
});
