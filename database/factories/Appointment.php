<?php

use App\Appointment;
use App\Shift;
use App\User;

$factory->define(Appointment::class, function (Faker\Generator $faker) {
    return [
        'cost' => $faker->numerify('###.##'),
        'shift_id' => fn() => factory(Shift::class)->create()->id,
        'starts_at' => $faker->dateTime,
        'user_id' => fn() => factory(User::class)->create()->id,
    ];
});
