<?php

$router->get('appointments', [
    'uses' => 'AppointmentsController@index',
]);
$router->post('appointments', [
    'middleware' => 'auth',
    'uses' => 'AppointmentsController@store',
]);
