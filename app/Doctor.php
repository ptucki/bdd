<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Doctor extends Model
{
    public function shifts(): HasMany
    {
        return $this->hasMany(Shift::class);
    }
}
