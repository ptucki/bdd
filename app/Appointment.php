<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Appointment extends Model
{
    public const LENGTH_MINUTES = 30;

    protected $dates = ['ends_at', 'starts_at'];

    public function shift(): BelongsTo {
        return $this->belongsTo(Shift::class);
    }

    public function user(): BelongsTo {
        return $this->belongsTo(User::class);
    }
}
