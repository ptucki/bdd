<?php
namespace App\Http\Controllers;

use App\Appointment;
use App\Doctor;
use App\Shift;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AppointmentsController extends Controller {
    public function index(Request $request): JsonResponse {
        $this->validate($request, [
            'date' => 'required|date_format:Y-m-d',
            'hour_after' => 'date_format:H:i',
            'hour_before' => 'date_format:H:i',
            'specialization' => 'required',
        ]);

        $date = $request->input('date');
        $hourAfter = $request->input('hour_after');
        $hourBefore = $request->input('hour_before');
        $specialization = $request->input('specialization');

        $shifts = Shift::with('doctor', 'appointments')
            ->whereDate('starts_at', $date)
            ->whereHas(
                'doctor',
                fn(Builder $query) => $query->where('specialization', $specialization),
            )
            ->get();

        $results = [];
        foreach ($shifts as $shift) {
            $shiftDoctor = $shift->doctor->name;
            for ($i = $shift->starts_at; $i->lt($shift->ends_at); $i = $i->addMinutes(Appointment::LENGTH_MINUTES)) {
                $dateAvailable = $shift->appointments
                    ->filter(fn(Appointment $appointment) => $appointment->starts_at->eq($i))
                    ->isEmpty();
                if ( ! $dateAvailable) {
                    continue;
                }
                if ($hourAfter && $i->lte($i->clone()->setHour($hourAfter))) {
                    continue;
                }
                if ($hourBefore && $i->gte($i->clone()->setHour($hourBefore))) {
                    continue;
                }
                $results[] = [
                    'doctor' => $shiftDoctor,
                    'date' => $i->format('Y-m-d H:i'),
                ];
            }
        }

        return new JsonResponse(['results' => $results]);
    }

    public function store(Request $request): JsonResponse {
        $this->validate($request, [
            'date_time' => 'required|date_format:Y-m-d H:i',
            'doctor' => 'required|exists:doctors,name',
        ]);

        $doctor = Doctor::whereName($request->input('doctor'))->firstOrFail();
        $appointmentDate = Carbon::make($request->input('date_time'));

        $shift = $doctor->shifts()
            ->whereDate('starts_at', $appointmentDate)
            ->where('starts_at', '<=', $appointmentDate)
            ->where('ends_at', '>=', $appointmentDate)
            ->firstOrFail();

        $user = $request->user();
        switch (strtolower($user->medical_plan)) {
            case 'basic':
                $cost = number_format($shift->cost, 2);
                break;
            case 'silver':
                $cost = '20.00';
                break;
            case 'gold':
                $cost = '0.00';
                break;
        }

        $appointment = new Appointment();
        $appointment->starts_at = $appointmentDate;
        $appointment->cost = $cost;
        $appointment->shift()->associate($shift);
        $appointment->user()->associate($user);
        $appointment->save();

        return new JsonResponse(['cost' => $appointment->cost]);
    }
}
