# [Projekt magisterski] Przeglądanie i rejestracja wizyt u lekarza 

Aplikacja oparta o micro-framework [Lumen](https://lumen.laravel.com/) umożliwiająca przeglądanie i rejestrację wizyt do lekarza. Publiczny interfejs aplikacji (REST API z dwoma punktami końcowymi HTTP) pokryty jest testami funkcjonalnymi bazującymi na scenariuszach w języku Gherkin, uruchamianymi z użyciem biblioteki BDD [Behat](https://behat.org).

Autor: Patryk Tucki <ptrktck@gmail.com>


## Wymagania

- [PHP7.4](https://www.php.net)
- [SQLite](https://www.sqlite.org/index.html)
- [Composer](https://getcomposer.org)

## Instalacja 

```
# Konfiguracja zmiennych środowiskowych aplikacji na podstawie przykładu.
cp .env.example .env
# Instalacja wymaganych bibliotek PHP.
composer install
# Utworzenie tabeli bazodanowych.
php artisan migrate
```

## Uruchomienie testów

```
vendor/bin/behat
```

## Ważne miejsca

- `features/*.feature` - pliki funkcjonalności zawierające scenariusze napisane w języku strukturalnym Gherkin po polsku,
- `features/bootstrap/FeatureContext.php` - implementacje poszczególnych kroków scenariuszy wykorzystujące metody pomocnicze micro-frameworka Lumen do testowania REST API ,
- `database/migrations/*.php` - migracje bazodanowe (tworzące tabele),
- `database/factories/*.php` - konfiguracje przykładowych wartości pól modeli bazodanowych na potrzeby testów,
- `app/*.php` - modele bazodanowe (abstrakcja tabel bazodanowych),
- `app/Http/Controllers/AppointmentsController.php` - właściwy kod aplikacji odpowiedzialny za działanie dwóch punktów końcowych HTTP umożliwiających przeglądanie i rejestrację wizyt u lekarza,
- `routes/web.php` - definicje ścieżek HTTP aplikacji.
