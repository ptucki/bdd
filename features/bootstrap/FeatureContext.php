<?php

use App\Appointment;
use App\Doctor;
use App\Shift;
use App\User;
use Behat\Behat\Context\Context;
use Behat\Behat\Hook\Scope\AfterScenarioScope;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Behat\Gherkin\Node\TableNode;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Laravel\Lumen\Testing\DatabaseMigrations;

class FeatureContext extends TestCase implements Context
{
    use DatabaseMigrations;

    /** @BeforeScenario */
    public function before(BeforeScenarioScope $scope)
    {
        parent::setUp();
    }

    /** @AfterScenario */
    public function after(AfterScenarioScope $scope)
    {
        parent::tearDown();
    }

    /**
     * @Given w dniu :arg1 dyżury pełnią:
     */
    public function wDniuDyzuryPelnia($arg1, TableNode $table)
    {
        foreach ($table->getIterator() as $tableRow) {
            $doctor = factory(Doctor::class)->create([
                'name' => $tableRow['lekarz'],
                'specialization' => $tableRow['specjalizacja'],
            ]);
            factory(Shift::class)->create([
                'cost' => $tableRow['koszt'],
                'doctor_id' => $doctor->id,
                'ends_at' => Carbon::make("{$arg1} {$tableRow['godzina zakonczenia']}"),
                'starts_at' => Carbon::make("{$arg1} {$tableRow['godzina rozpoczecia']}"),
            ]);
        }
    }

    /**
     * @Given następujące terminy są zajęte:
     */
    public function nastepujaceTerminySaZajete(TableNode $table)
    {
        foreach ($table->getIterator() as $tableRow) {
            $doctor = Doctor::whereName($tableRow['lekarz'])->firstOrFail();
            $date = Carbon::make($tableRow['termin']);
            factory(Appointment::class)->create([
                'shift_id' => $doctor->shifts()->whereDate('starts_at', $date)->firstOrFail()->id,
                'starts_at' => $date,
            ]);
        }
    }

    /**
     * @When interesuje mnie wizyta u :arg1 w dniu :arg2
     */
    public function interesujeMnieWizytaUWDniu($arg1, $arg2)
    {
        $this->get("appointments?specialization={$arg1}&date={$arg2}");
    }

    /**
     * @Then powinienem zobaczyć dostępne terminy:
     */
    public function powinienemZobaczycDostepneTerminy(TableNode $table)
    {
        $this->assertResponseOk();
        $this->seeJson([
            'results' => array_map(
                fn(array $tableRow) => [
                    'doctor' => $tableRow['lekarz'],
                    'date' => $tableRow['termin'],
                ],
                iterator_to_array($table->getIterator()),
            ),
        ]);
    }

    /**
     * @When interesuje mnie wizyta u :arg1 w dniu :arg2 przed godziną :arg3
     */
    public function interesujeMnieWizytaUWDniuPrzedGodzina($arg1, $arg2, $arg3)
    {
        $this->get("appointments?specialization={$arg1}&date={$arg2}&hour_before={$arg3}");
    }

    /**
     * @When interesuje mnie wizyta u :arg1 w dniu :arg2 po godzinie :arg3
     */
    public function interesujeMnieWizytaUWDniuPoGodzinie($arg1, $arg2, $arg3)
    {
        $this->get("appointments?specialization={$arg1}&date={$arg2}&hour_after={$arg3}");
    }

    /**
     * @Then powinienem zobaczyć potwierdzenie rezerwacji wizyty w cenie :arg1
     */
    public function powinienemZobaczycPotwierdzenieRezerwacjiWizytyWCenie($arg1)
    {
        $this->assertResponseOk();
        $this->seeJson(['cost' => $arg1]);
    }

    /**
     * @Given posiadam aktywny plan medyczny :arg1
     */
    public function posiadamAktywnyPlanMedyczny($arg1)
    {
        $user = factory(User::class)->create(['medical_plan' => $arg1]);
        $this->be($user, 'api');
    }

    /**
     * @When rezerwuję termin :arg1 u :arg2
     */
    public function rezerwujeTerminU($arg1, $arg2)
    {
        $this->post('appointments', [
            'date_time' => $arg1,
            'doctor' => $arg2,
        ]);
    }

    /**
     * @Then powinna istnieć w systemie rezerwacja terminu :arg1 u :arg2 w cenie :arg3
     */
    public function powinnaIstniecWSystemieRezerwacjaTerminuUWCenie($arg1, $arg2, $arg3)
    {
        $exists = Appointment::whereStartsAt(Carbon::make($arg1))
            ->where('cost', $arg3)
            ->whereHas('shift.doctor', fn(Builder $query) => $query->whereName($arg2))
            ->exists();

        $this->assertTrue($exists);
    }
}
