# language: pl
Funkcja: Przeglądanie terminów
    W celu rezerwacji wizyty u lekarza w dogodnym terminie
    Jako pacjent
    Potrzebuję mieć możliwość przeglądania dostępnych terminów

    Założenia:
        Zakładając, że w dniu "2020-01-10" dyżury pełnią:
            | lekarz             | specjalizacja | godzina rozpoczecia | godzina zakonczenia | koszt  |
            | Adam Nowak         | kardiolog     | 09:00               | 13:00               | 150.00 |
            | Krystyna Matejko   | okulista      | 11:00               | 15:00               | 200.00 |
            | Stanislaw Bielicki | urolog        | 08:00               | 14:00               | 250.00 |
        Oraz następujące terminy są zajęte:
            | lekarz             | termin           |
            | Adam Nowak         | 2020-01-10 10:00 |
            | Adam Nowak         | 2020-01-10 12:00 |
            | Krystyna Matejko   | 2020-01-10 14:00 |
            | Stanislaw Bielicki | 2020-01-10 9:30  |

    Scenariusz: Przeglądanie terminów w ciągu całego dnia
        Kiedy interesuje mnie wizyta u "kardiolog" w dniu "2020-01-10"
        Wtedy powinienem zobaczyć dostępne terminy:
            | lekarz     | termin           |
            | Adam Nowak | 2020-01-10 09:00 |
            | Adam Nowak | 2020-01-10 09:30 |
            | Adam Nowak | 2020-01-10 10:30 |
            | Adam Nowak | 2020-01-10 11:00 |
            | Adam Nowak | 2020-01-10 11:30 |
            | Adam Nowak | 2020-01-10 12:30 |

    Scenariusz: Przeglądanie terminów przed konkretną godziną
        Kiedy interesuje mnie wizyta u "urolog" w dniu "2020-01-10" przed godziną "10:00"
        Wtedy powinienem zobaczyć dostępne terminy:
            | lekarz             | termin           |
            | Stanislaw Bielicki | 2020-01-10 08:00 |
            | Stanislaw Bielicki | 2020-01-10 08:30 |
            | Stanislaw Bielicki | 2020-01-10 09:00 |

    Scenariusz: Przeglądanie terminów po konkretnej godzinie
        Kiedy interesuje mnie wizyta u "okulista" w dniu "2020-01-10" po godzinie "13:35"
        Wtedy powinienem zobaczyć dostępne terminy:
            | lekarz           | termin           |
            | Krystyna Matejko | 2020-01-10 14:30 |
