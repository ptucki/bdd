# language: pl
Funkcja: Rezerwacja wizyty
    W celu uzyskania bezpośredniej pomocy w związku z moją dolegliwością
    Jako pacjent
    Potrzebuję mieć możliwość rezerwacji wizyty u lekarza

    Szablon scenariusza: Rezerwacja wizyty
        Zakładając, że w dniu "2020-01-10" dyżury pełnią:
            | lekarz           | specjalizacja | godzina rozpoczecia | godzina zakonczenia | koszt  |
            | Krystyna Matejko | okulista      | 11:00               | 15:00               | 150.00 |
            | Adam Nowak       | urolog        | 08:00               | 14:00               | 200.00 |
            | Martyna Bak      | kardiolog     | 09:00               | 13:00               | 250.00 |
        Oraz posiadam aktywny plan medyczny "<plan>"
        Kiedy rezerwuję termin "<termin>" u "<lekarz>"
        Wtedy powinienem zobaczyć potwierdzenie rezerwacji wizyty w cenie "<faktyczny koszt>"
        Oraz powinna istnieć w systemie rezerwacja terminu "<termin>" u "<lekarz>" w cenie "<faktyczny koszt>"
        Przykłady:
            | plan   | lekarz           | termin           | faktyczny koszt |
            | Basic  | Krystyna Matejko | 2020-01-10 14:00 | 150.00          |
            | Basic  | Adam Nowak       | 2020-01-10 12:00 | 200.00          |
            | Basic  | Martyna Bak      | 2020-01-10 10:00 | 250.00          |
            | Silver | Krystyna Matejko | 2020-01-10 14:00 | 20.00           |
            | Silver | Adam Nowak       | 2020-01-10 12:00 | 20.00           |
            | Silver | Martyna Bak      | 2020-01-10 10:00 | 20.00           |
            | Gold   | Krystyna Matejko | 2020-01-10 14:00 | 0.00            |
            | Gold   | Adam Nowak       | 2020-01-10 12:00 | 0.00            |
            | Gold   | Martyna Bak      | 2020-01-10 10:00 | 0.00            |
